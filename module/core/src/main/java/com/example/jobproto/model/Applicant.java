package com.example.jobproto.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Applicant model class.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(
	name = "applicants")
public class Applicant implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 2619863608262186409L;

    @Id
    @GeneratedValue(
	    strategy = GenerationType.AUTO)
    private Long applicantId;

    @OneToOne(
	    cascade = CascadeType.ALL)
    User principal;

    private String firstName;

    private String midName;

    private String lastName;

    private String motto;

    private String avatar;

    @Column(
	    length = 1024)
    private String description;

    @ManyToMany(
	    fetch = FetchType.EAGER)
    private Set<Skill> skills;

    @OneToMany(
	    mappedBy = "applicatorId")
    private Set<Application> applications;

    @Transient
    private String fullName;

    public String getFullName() {
	StringBuilder fullname = new StringBuilder("");

	if (firstName != null && !firstName.isEmpty()) {
	    fullname.append(lastName);
	    fullname.append(" ");
	}
	if (midName != null && !midName.isEmpty()) {
	    fullname.append(midName);
	    fullname.append(" ");
	}
	if (firstName != null && !firstName.isEmpty()) {
	    fullname.append(firstName);
	}
	return fullname.toString();
    }
}
