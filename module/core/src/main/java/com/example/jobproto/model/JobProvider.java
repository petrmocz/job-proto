package com.example.jobproto.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Job provider entity
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(
	name = "job_provider")
@Entity
public class JobProvider implements Serializable {

    private static final long serialVersionUID = -3376046068181350641L;

    @Id
    @GeneratedValue(
	    strategy = GenerationType.AUTO)
    private Long providerId;

    private String providerName;

    private String providerDescription;

    @OneToMany(
	    cascade = CascadeType.ALL)
    // @PrimaryKeyJoinColumn
    Set<User> principals;

    @OneToMany(
	    cascade = CascadeType.ALL)
    @JoinColumn(
	    name = "providerId")
    Set<JobOffer> offers;
}
