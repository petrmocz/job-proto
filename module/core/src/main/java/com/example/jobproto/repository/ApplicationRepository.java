package com.example.jobproto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jobproto.model.Application;

/**
 * Application JPA repository
 */
@Repository
public interface ApplicationRepository
	extends JpaRepository<Application, Long> {

}
