package com.example.jobproto.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.jobproto.model.Applicant;
import com.example.jobproto.model.Application;
import com.example.jobproto.model.JobOffer;
import com.example.jobproto.model.Skill;
import com.example.jobproto.repository.ApplicantRepository;
import com.example.jobproto.repository.ApplicationRepository;
import com.example.jobproto.repository.JobOfferRepository;
import com.example.jobproto.repository.SkillRepository;

/**
 * Service used by Applicant entity
 */
@Service
public class ApplicantServiceImpl implements ApplicantService {

    @Autowired
    ApplicantRepository applicantRepository;

    @Autowired
    SkillRepository skillRepository;

    @Autowired
    JobOfferRepository jobOfferRepository;

    @Autowired
    ApplicationRepository applicationRepository;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.example.jobproto.service.ApplicantService#getApplicantByUsername(java
     * .lang.String)
     */
    @Override
    public Applicant getApplicantByUsername(String username) throws Exception {
	Applicant applicant = null;
	applicant = applicantRepository.findByPrincipalUsername(username);

	if (applicant == null) { throw new Exception("Applicant not found");

	}
	return applicant;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.example.jobproto.service.ApplicantService#addSkill(java.lang.String,
     * com.example.jobproto.model.Skill)
     */
    @Override
    public void addSkill(String username, Skill skill) throws Exception {
	// TODO Implement
	throw new Exception("Not implemented");

    }

    /*
     * (non-Javadoc)
     *
     * @see com.example.jobproto.service.ApplicantService#removeSkill(java.lang.
     * String, com.example.jobproto.model.Skill)
     */
    @Override
    public void removeSkill(String username, Skill skill) throws Exception {
	// TODO Implement
	throw new Exception("Not implemented");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.example.jobproto.service.ApplicantService#apply(java.lang.String,
     * com.example.jobproto.model.Application)
     */
    @Override
    public void apply(String username, Application application)
	    throws Exception {
	// TODO Implement
	throw new Exception("Not implemented");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.example.jobproto.service.ApplicantService#getUserJobOffers(java.lang.
     * String)
     */
    @Override
    public Collection<JobOffer> getUserJobOffers(String username)
	    throws Exception {
	// Applicant applicant =
	// applicantRepository.findByPrincipalUsername(username);
	Collection<JobOffer> jobOffers = jobOfferRepository.findAll();
	return jobOffers;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.example.jobproto.service.ApplicantService#getUserApplications(java.
     * lang.String)
     */
    @Override
    public Collection<Application> getUserApplications(String username)
	    throws Exception {
	Collection<Application> applications = null;
	Applicant applicant =
		applicantRepository.findByPrincipalUsername(username);
	if (applicant == null) {
	    throw new Exception("Applicant not found");
	} else {
	    applications = applicant.getApplications();
	}
	return applications;
    }

}
