package com.example.jobproto.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO: comment this
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(
	name = "job_offers")
@Entity
public class JobOffer implements Serializable {

    /**
     * Generated serial Id
     */
    private static final long serialVersionUID = 1533177601096999719L;

    @Id
    @GeneratedValue(
	    strategy = GenerationType.AUTO)
    private Long offerId;

    @ManyToOne
    private JobProvider provider;

    private String jobName;

    private String jobDescription;

    @ManyToMany(
	    fetch = FetchType.EAGER)
    private Set<Skill> requiredSkills;

    @OneToMany(
	    mappedBy = "offerId")
    private Set<Application> applications;

}
