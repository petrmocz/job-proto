package com.example.jobproto.service;

/**
 * Simple service.
 *
 */
public interface TestService {

    /**
     * Testing method for test service.
     *
     * @return simple text message
     */
    public String getMessage();
}
