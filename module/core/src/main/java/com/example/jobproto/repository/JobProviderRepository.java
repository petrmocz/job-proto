package com.example.jobproto.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jobproto.model.JobProvider;

/**
 * JobProvider JPA repository
 */
@Repository
public interface JobProviderRepository
	extends JpaRepository<JobProvider, Long> {
    Optional<JobProvider> findByProviderName(String providerName);
}
