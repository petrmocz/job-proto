package com.example.jobproto.model;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Principal class.
 *
 */
@Data
@Builder
// @NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(
	name = "USERS")
public class User implements UserDetails {

    private static final long serialVersionUID = 2745322835003117242L;

    @Id
    private String username;

    private String password;

    private Boolean enabled;

    @Builder.Default
    @Transient
    private Boolean locked = false;

    @Builder.Default
    @Transient
    private Boolean expired = false;

    // TODO rewrite to set
    String role;

    /**
     *
     * @param username
     * @param password
     * @param enabled
     * @param role
     */
    public User(String username, String password, boolean enabled,
	    String role) {
	this.username = username;
	this.password = password;
	this.enabled = enabled;
	this.role = role;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.security.core.userdetails.UserDetails#getAuthorities(
     * )
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
	// TODO rewrite to set

	return Stream.of(new SimpleGrantedAuthority(role))
		.collect(Collectors.toSet());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.core.userdetails.UserDetails#
     * isAccountNonExpired()
     */
    @Override
    public boolean isAccountNonExpired() {
	return this.expired;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.core.userdetails.UserDetails#
     * isAccountNonLocked()
     */
    @Override
    public boolean isAccountNonLocked() {
	return this.locked;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.core.userdetails.UserDetails#
     * isCredentialsNonExpired()
     */
    @Override
    public boolean isCredentialsNonExpired() {
	return this.expired;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.security.core.userdetails.UserDetails#isEnabled()
     */
    @Override
    public boolean isEnabled() {
	return this.enabled;
    }

}
