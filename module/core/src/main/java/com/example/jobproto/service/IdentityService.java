/**
 *
 */
package com.example.jobproto.service;

import java.security.Principal;

/**
 * Test service
 */
public interface IdentityService {

    public boolean validate(Principal principal);
}
