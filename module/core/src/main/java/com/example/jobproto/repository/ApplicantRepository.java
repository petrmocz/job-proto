/**
 *
 */
package com.example.jobproto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jobproto.model.Applicant;

/**
 * Applicant JPA repository
 *
 */
@Repository
public interface ApplicantRepository extends JpaRepository<Applicant, Long> {
    Applicant findByPrincipalUsername(String username);

    /**
     * @param string
     * @return
     */
    Applicant findByLastName(String string);
}
