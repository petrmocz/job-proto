package com.example.jobproto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jobproto.model.User;

/**
 * Principal repository.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username);

}
