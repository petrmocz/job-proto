package com.example.jobproto.service;

import java.util.Collection;

import com.example.jobproto.model.Applicant;
import com.example.jobproto.model.Application;
import com.example.jobproto.model.JobOffer;
import com.example.jobproto.model.Skill;

/**
 * Applicant service desc.
 */
public interface ApplicantService {

    /**
     * Query for Applicant details by username
     *
     * @param username
     *            username
     * @return @see Applicant
     * @throws Exception
     */
    public Applicant getApplicantByUsername(String username) throws Exception;

    /**
     * Add skill to applicant
     *
     * @param username
     *            username
     * @param skill
     *            com.example.jobproro.model.Skill
     *
     * @throws Exception
     *
     */
    public void addSkill(String username, Skill skill) throws Exception;

    /**
     * Remove skill from applicant
     *
     * @param username
     *            username
     * @param skill
     *            com.example.jobproro.model.Skill
     *
     * @throws Exception
     *
     */
    public void removeSkill(String username, Skill skill) throws Exception;

    /**
     * apply to application
     *
     * @param username
     *            Username
     * @param application
     *            {@link com.example.jobproto.model.Application}
     * @throws Exception
     */
    public void apply(String username, Application application)
	    throws Exception;

    public Collection<JobOffer> getUserJobOffers(String username)
	    throws Exception;

    /**
     * get applications for given user
     *
     * @param username
     *            Username
     * @return Collection of {@link com.example.jobproto.model.Application}
     *
     * @throws Exception
     */
    public Collection<Application> getUserApplications(String username)
	    throws Exception;

}
