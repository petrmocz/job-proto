package com.example.jobproto;

import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.jobproto.model.Applicant;
import com.example.jobproto.model.JobOffer;
import com.example.jobproto.model.JobProvider;
import com.example.jobproto.model.Skill;
import com.example.jobproto.model.User;
import com.example.jobproto.repository.ApplicantRepository;
import com.example.jobproto.repository.ApplicationRepository;
import com.example.jobproto.repository.JobOfferRepository;
import com.example.jobproto.repository.JobProviderRepository;
import com.example.jobproto.repository.SkillRepository;

import lombok.extern.java.Log;

/**
 * Application sample data initializing.
 */

@Profile("dev")
@Component
@Log
public class ApplicationSampleData {

    @Autowired
    SkillRepository skillRepository;

    @Autowired
    ApplicantRepository applicantRepository;

    @Autowired
    JobProviderRepository jobProviderRepository;

    @Autowired
    JobOfferRepository jobOfferRepository;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    PasswordEncoder encoder;

    // @Autowired
    // UserRepository principalRepository;

    @Bean
    InitializingBean loadData() {
	return () -> {
	    loadSkillData();
	    loadApplicantData();
	    loadProviderData();
	    loadOfferData();
	    loadApplicationData();
	};

    }

    /**
     * Load applicant sample data.
     */
    private void loadApplicantData() {
	log.info("Creating sample applicant data");
	Stream.of(
		Applicant.builder()
			.principal(new User("ap", encoder.encode("ap"), true,
				"USER"))
			.lastName("Andrew").firstName("Pamela")
			.motto("Keep it simple and clean")
			.avatar("/img/assets/007.png")
			.skills(Stream.of(
				skillRepository.findBySkillName("HTML").orElse(
					null),
				skillRepository.findBySkillName(
					"CSS").orElse(
						null))
				.collect(Collectors.toCollection(HashSet::new)))
			.description(
				"Pamela, in full Pamela; or, Virtue Rewarded, novel in epistolary style by Samuel Richardson, published in 1740 and based on a story about a servant and the man who, failing to seduce her, marries her.")
			.build(),
		Applicant.builder()
			.principal(new User("ba", encoder.encode("ba"), true,
				"USER"))

			.lastName("Bret").firstName("Ashley")
			.motto("The sun also rises")
			.avatar("img/assets/011.png")
			.skills(Stream.of(
				skillRepository.findBySkillName("Java")
					.orElse(null),
				skillRepository.findBySkillName("Javascript")
					.orElse(null),
				skillRepository.findBySkillName("HTML")
					.orElse(null))
				.collect(Collectors.toCollection(HashSet::new)))
			.description(
				"fictional character, one of the principal characters of Ernest Hemingway’s novel The Sun Also Rises (1926). An expatriate Englishwoman in Paris during the 1920s, she is typical of the Lost Generation of men and women whose lives have no focus or meaning and who therefore wander aimlessly from one party to another.")
			.build())
		.forEach(e -> {
		    applicantRepository.save(e);
		    applicantRepository.flush();
		});

    }

    /**
     * Load skill sample data.
     */
    private void loadSkillData() {
	log.info("Creating sample skills data");
	Stream.of(
		Skill.builder().skillName("HTML")
			.skillDescription("Hypertext Markup Language").build(),
		Skill.builder().skillName("Java").skillDescription(
			"Java is a general-purpose computer-programming language that is concurrent, "
				+ "class-based, object-oriented,[15] and specifically designed to have as few "
				+ "implementation dependencies as possible.")
			.build(),
		Skill.builder().skillName("Spring Framework").skillDescription(
			"The Spring Framework is an application framework and inversion of control "
				+ "container for the Java platform.")
			.build(),
		Skill.builder().skillName("CSS")
			.skillDescription("Cascading Style Sheets (CSS)")
			.build(),
		Skill.builder().skillName("JavaScript").skillDescription(
			"JavaScript often abbreviated as JS, is a high-level, interpreted programming "
				+ "language. ")
			.build(),
		Skill.builder().skillName("Angular").skillDescription(
			"Angular (commonly referred to as \"Angular 2+\" or \"Angular v2 and above\")[4][5] "
				+ "is a TypeScript-based open-source front-end web application platform ")
			.build(),
		Skill.builder().skillName("Apache Wicket").skillDescription(
			"Apache Wicket, commonly referred to as Wicket, is a lightweight component-based web"
				+ " application framework for the Java programming language")
			.build())
		.forEach(e -> skillRepository.save(e));

    }

    /**
     * Load application sample data
     */
    private void loadApplicationData() {
	log.warning("Create application sample data");
    }

    /**
     * Load sample provider data.
     */
    private void loadProviderData() {
	log.info("Create sample provider data");
	Stream.of(
		JobProvider.builder().providerName("Big company")
			.providerDescription("Big company")
			.principals(Stream
				.of(new User("bc", encoder.encode("bc"), true,
					"PROVIDER"))
				.collect(Collectors.toSet()))
			.build(),
		JobProvider.builder().providerName("Startup company Ltd.")
			.providerDescription("Small company")
			.principals(Stream
				.of(new User("sc1", encoder.encode("sc1"), true,
					"PROVIDER"),
					new User("sc2", encoder.encode("sc2"),
						true, "PROVIDER"))
				.collect(Collectors.toSet()))
			.build())
		.forEach(e -> jobProviderRepository.save(e));

    }

    /**
     * Load sample job offers data.
     */
    private void loadOfferData() {
	log.info("Create job offer sample data");
	Stream.of(JobOffer.builder().jobName("HTML coder")
		.provider(jobProviderRepository
			.findByProviderName("Big company").orElse(null))
		.jobDescription("Wanted good FE developer")
		.requiredSkills(Stream.of(
			skillRepository.findBySkillName("HTML").orElse(null),
			skillRepository.findBySkillName("CSS").orElse(null))
			.collect(Collectors.toCollection(HashSet::new)))
		.build(),
		JobOffer.builder().jobName("JAVA developer")
			.provider(jobProviderRepository
				.findByProviderName("Big company").orElse(null))
			.jobDescription("Experienced Java Developer wanted")
			.requiredSkills(Stream.of(
				skillRepository.findBySkillName("Java")
					.orElse(null),
				skillRepository.findBySkillName("Apache Wicket")
					.orElse(null))
				.collect(Collectors.toCollection(HashSet::new)))
			.build(),
		JobOffer.builder().jobName("Tester Junior")
			.provider(jobProviderRepository
				.findByProviderName("Startup company Ltd.")
				.orElse(null))
			.jobDescription("Experinced Java Developer wanted")
			.requiredSkills(Stream.of(
				skillRepository.findBySkillName("HTML")
					.orElse(null),
				skillRepository.findBySkillName("JavaScript")
					.orElse(null))
				.collect(Collectors.toCollection(HashSet::new)))
			.build())
		.forEach(e -> jobOfferRepository.save(e));
    }
}
