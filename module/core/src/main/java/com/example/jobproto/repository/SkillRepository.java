package com.example.jobproto.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jobproto.model.Skill;

/**
 * Skill JPA repository.
 *
 */
@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {

    public Optional<Skill> findBySkillName(String skillName);

}
