package com.example.jobproto.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Skill class.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(
	name = "skills")
public class Skill implements Serializable {

    private static final long serialVersionUID = -888703719951386049L;

    @Id
    private String skillName;

    private String skillDescription;

}
