package com.example.jobproto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jobproto.model.JobOffer;

/**
 * Job Offers JPA repository
 *
 */
@Repository
public interface JobOfferRepository extends JpaRepository<JobOffer, Long> {

}
