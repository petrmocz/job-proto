/**
 *
 */
package com.example.jobproto.service;

import org.springframework.stereotype.Service;

/**
 * Very simple service.
 */
@Service
public class SimpleTestService implements TestService {

    @Override
    public String getMessage() {
	return "Service message";
    }

}
