/**
 *
 */
package com.example.jobproto.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.example.jobproto.model.User;
import com.example.jobproto.repository.UserRepository;

/**
 * UserDetailService implementation using JPA Repository.
 *
 */
public class JpaUserDetailService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.core.userdetails.UserDetailsService#
     * loadUserByUsername(java.lang.String)
     */
    @Override
    public UserDetails loadUserByUsername(String username)
	    throws UsernameNotFoundException {

	User user = userRepository.findByUsername(username);

	Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
	grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole()));

	return new org.springframework.security.core.userdetails.User(
		user.getUsername(), user.getPassword(), grantedAuthorities);

    }

}
