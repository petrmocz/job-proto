/**
 *
 */
package com.example.jobproto.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO: comment this
 *
 * @since 2018-10-09
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(
	name = "application")
@Entity
public class Application {

    @Id
    @GeneratedValue(
	    strategy = GenerationType.AUTO)
    private Long applicationId;

    private Long applicatorId;

    private Long offerId;

    // private Applicant applicant;

}
