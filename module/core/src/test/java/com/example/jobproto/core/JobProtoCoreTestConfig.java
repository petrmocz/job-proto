package com.example.jobproto.core;

import org.springframework.boot.SpringBootConfiguration;

/**
 * Spring config class for unit testing.
 */
// @Configuration
@SpringBootConfiguration
public class JobProtoCoreTestConfig {

}
