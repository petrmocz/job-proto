package com.example.jobproto.core.model;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.jobproto.model.User;

/**
 * User unit test.
 *
 */
@RunWith(SpringRunner.class)
public class UserTest {

    private final String USERNAME = "USERNAME";
    private final String PASSWORD = "PASSWORD";
    private final boolean ENABLED = true;
    private final String ROLE = "ROLE";
    private final String DUMMYSTR = "DUMMY";
    private User user;

    @Before
    public void init() {
	user = new User(USERNAME, PASSWORD, ENABLED, ROLE);
    }

    /**
     * Test method for {@link com.example.jobproto.model.User#getAuthorities()}.
     */
    // @Ignore
    @Test
    public void testGetAuthorities() {
	GrantedAuthority authority = new SimpleGrantedAuthority(ROLE);
	assertThat(user.getAuthorities(), Matchers.hasSize(1));
	assertThat(user.getAuthorities(), Matchers.contains(authority));
    }

    /**
     * Test method for {@link com.example.jobproto.model.User#getUsername()}.
     */
    @Test
    public void testGetUsername() {
	assertEquals(user.getUsername(), USERNAME);
    }

    /**
     * Test method for {@link com.example.jobproto.model.User#getPassword()}.
     */
    @Test
    public void testGetPassword() {
	assertEquals(user.getPassword(), PASSWORD);
    }

    /**
     * Test method for {@link com.example.jobproto.model.User#getEnabled()}.
     */
    @Test
    public void testGetEnabled() {
	assertEquals(user.getEnabled(), ENABLED);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.User#setUsername(String)}.
     */
    @Test
    public void testSetUsername() {
	String saveStr = user.getUsername();
	user.setUsername(DUMMYSTR);
	assertEquals(user.getUsername(), DUMMYSTR);
	user.setUsername(saveStr);
	assertEquals(user.getUsername(), USERNAME);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.User#setPassword(String)}.
     */
    @Test
    public void testSetPassword() {
	String saveStr = user.getPassword();
	user.setPassword(DUMMYSTR);
	assertEquals(user.getPassword(), DUMMYSTR);
	user.setPassword(saveStr);
	assertEquals(user.getPassword(), PASSWORD);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.User#setEnabled(Boolean)}.
     */
    @Test
    public void testSetEnabled() {
	Boolean saveBool = user.getEnabled();
	user.setEnabled(false);
	assertEquals(false, user.getEnabled());
	user.setEnabled(saveBool);
	assertEquals(user.getEnabled(), ENABLED);
    }

    /**
     * Test method for {@link com.example.jobproto.model.User#setRole(String)}.
     */
    @Test
    public void testSetRole() {
	String saveStr = user.getRole();
	user.setRole(DUMMYSTR);
	assertEquals(user.getRole(), DUMMYSTR);
	user.setRole(saveStr);
	assertEquals(user.getRole(), ROLE);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.User#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObject() {
	User copy = user;
	assertEquals(user.equals(copy), true);
    }

    /**
     * Test method for {@link com.example.jobproto.model.User#toString()}.
     */
    @Test
    public void testToString() {
	assertNotNull(user.toString());
	assertThat(user.toString(), containsString(USERNAME));
    }

}
