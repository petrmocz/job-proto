package com.example.jobproto.core.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import com.example.jobproto.model.Skill;

/**
 * Skill model unit tests.
 *
 */
public class SkillTest {

    private final String SKILLNAME = "SKILL";
    private final String SKILLDESC = "DESCRIPTION";

    private final String DUMMYSTR = "DUMMY";

    private Skill skill;

    @Before
    public void init() {
	skill = new Skill(SKILLNAME, SKILLDESC);
    }

    /**
     * Test method for {@link com.example.jobproto.model.Skill#getSkillName()}.
     */
    @Test
    public void testGetSkillName() {
	assertEquals(SKILLNAME, skill.getSkillName());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Skill#getSkillDescription()}.
     */
    @Test
    public void testGetSkillDescription() {
	assertEquals(SKILLDESC, skill.getSkillDescription());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Skill#setSkillName(String)}.
     */
    @Test
    public void testSetSkillName() {
	String saveStr = skill.getSkillName();
	skill.setSkillName(DUMMYSTR);
	assertEquals(DUMMYSTR, skill.getSkillName());
	skill.setSkillName(saveStr);
	assertEquals(SKILLNAME, skill.getSkillName());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Skill#setSkillDescription(String)}.
     */
    @Test
    public void testSetSkillDescription() {
	String saveStr = skill.getSkillDescription();
	skill.setSkillDescription(DUMMYSTR);
	assertEquals(DUMMYSTR, skill.getSkillDescription());
	skill.setSkillDescription(saveStr);
	assertEquals(SKILLDESC, skill.getSkillDescription());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Skill#Skill(String, String)}.
     */
    @Test
    public void testSkillStringString() {
	Skill newSkill = new Skill(DUMMYSTR, DUMMYSTR);
	assertEquals(DUMMYSTR, newSkill.getSkillName());
	assertEquals(DUMMYSTR, newSkill.getSkillDescription());
    }

    /**
     * Test method for {@link com.example.jobproto.model.Skill#Skill()}.
     */
    @Test
    public void testSkill() {
	Skill newSkill = new Skill();
	assertThat(newSkill.getSkillName(), Matchers.nullValue());
	assertThat(newSkill.getSkillDescription(), Matchers.nullValue());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Skill#equals}.
     */
    @Test
    public void testSkillEquals() {
	Skill newSkill = new Skill(SKILLNAME, SKILLDESC);
	assertTrue(skill.equals(newSkill));
    }

}
