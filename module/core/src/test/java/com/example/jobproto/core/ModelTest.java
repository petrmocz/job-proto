package com.example.jobproto.core;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.jobproto.model.Applicant;
import com.example.jobproto.model.JobProvider;
import com.example.jobproto.model.User;
import com.example.jobproto.repository.ApplicantRepository;
import com.example.jobproto.repository.ApplicationRepository;
import com.example.jobproto.repository.JobOfferRepository;
import com.example.jobproto.repository.JobProviderRepository;

/**
 * Model test.
 */
@RunWith(SpringRunner.class)
@EnableJpaRepositories(
	basePackages = { "com.example.jobproto.repository" })
@EntityScan(
	basePackages = { "com.example.jobproto.model" })
@DataJpaTest
public class ModelTest implements ApplicationContextAware {

    private static Logger logger = LoggerFactory.getLogger(ModelTest.class);

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ApplicantRepository applicantRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private JobProviderRepository jobProviderRepository;

    @Autowired
    private JobOfferRepository jobOfferRepository;

    private ApplicationContext applicationContext;

    @Test
    public void applicantRepositoryTest() {
	assertThat(applicantRepository, notNullValue());
    }

    @Test
    public void applicationRepositoryTest() {
	assertThat(applicationRepository, notNullValue());
    }

    @Test
    public void jobProviderRepositoryTest() {
	assertThat(jobProviderRepository, notNullValue());
    }

    @Test
    public void jobOfferRepositoryTest() {
	assertThat(jobOfferRepository, notNullValue());
    }

    @Test
    public void applicantStoreTest() {
	final String NAME = "jon";
	entityManager.persist(Applicant.builder().lastName(NAME)
		.principal(new User(NAME, "", true, null)).build());

	entityManager.flush();
	// List<Applicant> aa = applicantRepository.findAll();
	// logger.warn("{}", aa.size());
	// aa.stream().forEach(e -> logger.warn("{}", e.toString()));
	Applicant a = applicantRepository.findByLastName(NAME);
	assertThat(a, notNullValue());
	assertThat(a.getLastName().equals(NAME), is(true));

	Applicant b = applicantRepository.findByPrincipalUsername(NAME);
	assertThat(b, notNullValue());
	assertThat(b.getPrincipal().getUsername().equals(NAME), is(true));
    }

    @Test
    public void jopProviderStoreTest() {
	final String NAME = "BIG COMPANY";
	entityManager.persist(JobProvider.builder().providerName(NAME).build());

	entityManager.flush();
	// List<JobProvider> aa = jobProviderRepository.findAll();
	// logger.warn("{}", aa.size());
	// aa.stream().forEach(e -> logger.warn("{}", e.toString()));
	JobProvider a = jobProviderRepository.findByProviderName(NAME).get();
	assertThat(a, notNullValue());
	assertThat(a.getProviderName().equals(NAME), is(true));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.context.ApplicationContextAware#setApplicationContext
     * (org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
	    throws BeansException {
	this.applicationContext = applicationContext;

    }
}
