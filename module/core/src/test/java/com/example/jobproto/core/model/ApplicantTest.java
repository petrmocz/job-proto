package com.example.jobproto.core.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.jobproto.model.Applicant;
import com.example.jobproto.model.Skill;
import com.example.jobproto.model.User;

/**
 * Tests for Applicant class.
 *
 */
@RunWith(SpringRunner.class)
public class ApplicantTest {

    private final Long ID = 100L;
    private final String FIRSTNAME = "FirtName";
    private final String MIDNAME = "MidName";
    private final String LASTNAME = "LastName";
    private final String AVATAR = "N/A";
    private final String MOTTO = "Motto";
    private final String DESCRIPTION = "Description";
    private final User USER = new User("USERNAME", "PASSWORD", true, "USER");
    private final Set<Skill> SKILLS =
	    new HashSet<Skill>(Arrays.asList(new Skill("SKILL", "SKILL DESC")));
    private final Long DUMMYLONG = 101L;
    private final String DUMMYSTRING = "DUMMY";
    private Applicant applicant;

    @Before
    public void init() {
	applicant =
		Applicant.builder().applicantId(ID).firstName(FIRSTNAME)
			.midName(MIDNAME).lastName(LASTNAME).avatar(AVATAR)
			.motto(MOTTO).description(DESCRIPTION).principal(USER)
			.skills(SKILLS).build();
    }

    /**
     * Test method for {@link com.example.jobproto.model.Applicant#hashCode()}.
     */
    @Test
    public void testHashCode() {
	Applicant copy = applicant;
	assertEquals(applicant.hashCode(), copy.hashCode());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getFullName()}.
     */
    @Test
    public void testGetFullName() {
	// TODO:
	assertThat(applicant.getFullName(),
		is(LASTNAME + " " + MIDNAME + " " + FIRSTNAME));
    }

    /**
     * Test method for {@link com.example.jobproto.model.Applicant#builder()}.
     */
    @Ignore
    @Test
    public void testBuilder() {
	fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getApplicantId()}.
     */
    @Test
    public void testGetApplicantId() {
	assertEquals(applicant.getApplicantId(), ID);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getPrincipal()}.
     */
    @Test
    public void testGetPrincipal() {
	assertEquals(applicant.getPrincipal().getUsername(),
		USER.getUsername());
	assertEquals(applicant.getPrincipal().getPassword(),
		USER.getPassword());
	assertEquals(applicant.getPrincipal().getRole(), USER.getRole());
	assertEquals(applicant.getPrincipal().getEnabled(), USER.getEnabled());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getFirstName()}.
     */
    @Test
    public void testGetFirstName() {
	assertEquals(applicant.getFirstName(), FIRSTNAME);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getMidName()}.
     */
    @Test
    public void testGetMidName() {
	assertEquals(applicant.getMidName(), MIDNAME);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getLastName()}.
     */
    @Test
    public void testGetLastName() {
	assertEquals(applicant.getLastName(), LASTNAME);
    }

    /**
     * Test method for {@link com.example.jobproto.model.Applicant#getMotto()}.
     */
    @Test
    public void testGetMotto() {
	assertEquals(applicant.getMotto(), MOTTO);
    }

    /**
     * Test method for {@link com.example.jobproto.model.Applicant#getAvatar()}.
     */
    @Test
    public void testGetAvatar() {
	assertEquals(applicant.getAvatar(), AVATAR);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getDescription()}.
     */
    @Test
    public void testGetDescription() {
	assertEquals(applicant.getDescription(), DESCRIPTION);
    }

    /**
     * Test method for {@link com.example.jobproto.model.Applicant#getSkills()}.
     */
    @Test
    public void testGetSkills() {
	assertEquals(applicant.getSkills(), SKILLS);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#getApplications()}.
     */
    @Ignore
    @Test
    public void testGetApplications() {
	fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setApplicantId(Long)}.
     */
    @Test
    public void testSetApplicantId() {
	Long saveId = applicant.getApplicantId();
	applicant.setApplicantId(DUMMYLONG);
	assertEquals(applicant.getApplicantId(), DUMMYLONG);
	applicant.setApplicantId(saveId);
	assertEquals(applicant.getApplicantId(), ID);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setPrincipal(User)}.
     */
    @Ignore
    @Test
    public void testSetPrincipal() {
	fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setFirstName(String)}.
     */
    @Test
    public void testSetFirstName() {
	String saveStr = applicant.getFirstName();
	applicant.setFirstName(DUMMYSTRING);
	assertEquals(applicant.getFirstName(), DUMMYSTRING);
	applicant.setFirstName(saveStr);
	assertEquals(applicant.getFirstName(), FIRSTNAME);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setMidName(String)}.
     */
    @Test
    public void testSetMidName() {
	String saveStr = applicant.getMidName();
	applicant.setMidName(DUMMYSTRING);
	assertEquals(applicant.getMidName(), DUMMYSTRING);
	applicant.setMidName(saveStr);
	assertEquals(applicant.getMidName(), MIDNAME);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setLastName(String)}.
     */
    @Test
    public void testSetLastName() {
	String saveStr = applicant.getLastName();
	applicant.setLastName(DUMMYSTRING);
	assertEquals(applicant.getLastName(), DUMMYSTRING);
	applicant.setLastName(saveStr);
	assertEquals(applicant.getLastName(), LASTNAME);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setMotto(String)}.
     */
    @Test
    public void testSetMotto() {
	String saveStr = applicant.getMotto();
	applicant.setMotto(DUMMYSTRING);
	assertEquals(applicant.getMotto(), DUMMYSTRING);
	applicant.setMotto(saveStr);
	assertEquals(applicant.getMotto(), MOTTO);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setAvatar(String)}.
     */
    @Test
    public void testSetAvatar() {
	String saveStr = applicant.getAvatar();
	applicant.setAvatar(DUMMYSTRING);
	assertEquals(applicant.getAvatar(), DUMMYSTRING);
	applicant.setAvatar(saveStr);
	assertEquals(applicant.getAvatar(), AVATAR);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setDescription(String)}.
     */
    @Test
    public void testSetDescription() {
	String saveStr = applicant.getDescription();
	applicant.setDescription(DUMMYSTRING);
	assertEquals(applicant.getDescription(), DUMMYSTRING);
	applicant.setDescription(saveStr);
	assertEquals(applicant.getDescription(), DESCRIPTION);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setSkills(Set)}.
     */
    @Ignore
    @Test
    public void testSetSkills() {
	fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setApplications(Set)}.
     */
    @Ignore
    @Test
    public void testSetApplications() {
	fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#setFullName(String)}.
     */
    @Ignore
    @Test
    public void testSetFullName() {
	fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObject() {
	Applicant copy = applicant;
	assertEquals(applicant, copy);
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#canEqual(java.lang.Object)}.
     */
    @Test
    public void testCanEqual() {
	Applicant copy = applicant;
	assertTrue(applicant.equals(copy));
    }

    /**
     * Test method for {@link com.example.jobproto.model.Applicant#toString()}.
     */
    @Test
    public void testToString() {
	assertNotNull(applicant.toString());
    }

    /**
     * Test method for
     * {@link com.example.jobproto.model.Applicant#Applicant(Long, User, String, String, String, String, String, String, Set, Set, String)}.
     */
    @Ignore
    @Test
    public void testApplicantLongUserStringStringStringStringStringStringSetOfSkillSetOfApplicationString() {
	fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.example.jobproto.model.Applicant#Applicant()}.
     */
    @Ignore
    @Test
    public void testApplicant() {
	fail("Not yet implemented");
    }

}
