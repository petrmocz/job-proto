package com.example.jobproto.ui;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Simple test using the WicketTester
 */
@Ignore
public class TestTemplatePage {
    private WicketTester tester;

    @Before
    public void setUp() {
	tester = new WicketTester(new WicketApplication());
    }

    @Test
    public void homepageRendersSuccessfully() {
	// start and render the test page
	tester.startPage(TemplatePage.class);

	// assert rendered page class
	tester.assertRenderedPage(TemplatePage.class);
    }
}
