package com.example.jobproto.ui;

import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.stereotype.Component;
import org.wicketstuff.annotation.scan.AnnotatedMountScanner;

import com.example.jobproto.auth.SecureWebSession;
import com.example.jobproto.ui.pages.LoginPage;

/**
 * Application object for Job Proto web application.
 *
 */
@Component
public class WicketApplication extends AuthenticatedWebApplication {

    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends WebPage> getHomePage() {
	return TemplatePage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
	super.init();

	// https://www.mkyong.com/wicket/wicket-spring-integration-example/
	getComponentInstantiationListeners()
		.add(new SpringComponentInjector(this));
	getRequestCycleSettings().setResponseRequestEncoding("UTF-8");
	getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
	new AnnotatedMountScanner().scanPackage("com.example").mount(this);
	// getSecuritySettings().setAuthorizationStrategy(
	// new AnnotationsRoleAuthorizationStrategy(this));

    }

    @Override
    protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
	return SecureWebSession.class;
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
	return LoginPage.class;
    }
}
