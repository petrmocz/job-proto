package com.example.jobproto.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

/**
 * Security web session class.
 *
 * https://github.com/dmbeer/wicket-7-spring-security/blob/master/src/main/java/com/copperarrow/auth/SecureWebSession.java
 *
 */
@SuppressWarnings("serial")
public class SecureWebSession extends AuthenticatedWebSession {

    private HttpSession httpSession;

    @SpringBean(
	    name = "authenticationManger")
    private AuthenticationManager authenticationManager;

    /**
     * @param request
     */
    public SecureWebSession(Request request) {
	super(request);
	this.httpSession =
		((HttpServletRequest) request.getContainerRequest())
			.getSession();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.apache.wicket.authroles.authentication.AuthenticatedWebSession#
     * authenticate(java.lang.String, java.lang.String)
     */
    @Override
    protected boolean authenticate(String username, String password) {
	Authentication auth =
		authenticationManager.authenticate(
			new UsernamePasswordAuthenticationToken(username,
				password));
	if (auth.isAuthenticated()) {
	    SecurityContextHolder.getContext().setAuthentication(auth);
	    httpSession.setAttribute(
		    HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
		    SecurityContextHolder.getContext());
	    return true;
	} else {
	    return false;

	}
    }

    /*
     * (non-Javadoc)
     *
     * @see org.apache.wicket.authroles.authentication.
     * AbstractAuthenticatedWebSession#getRoles()
     */
    @Override
    public Roles getRoles() {
	throw new UnsupportedOperationException("Not supported yet.");
    }

}
