package com.example.jobproto.ui.base;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.example.jobproto.repository.ApplicantRepository;

/**
 * Card deck container class.
 *
 */
public class CardDeck extends Panel {

    @SpringBean
    ApplicantRepository applicantRepository;

    /**
     * @param id
     */
    public CardDeck(String id) {
	super(id);

	RepeatingView cardList = new RepeatingView("cardList");

	applicantRepository.findAll().forEach(e -> {
	    cardList.add(new ApplicantCard(cardList.newChildId(), Model.of(e)));
	});

	add(cardList);
    }

}
