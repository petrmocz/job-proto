package com.example.jobproto.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.jobproto.service.JpaUserDetailService;

/**
 *
 * Security related configuration class.
 *
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    // @Bean
    // @Override
    // public AuthenticationManager authenticationManager() throws Exception {
    // return super.authenticationManager();
    // }
    @Override
    @Bean(
	    name = "authenticationManager")
    public AuthenticationManager authenticationManagerBean() throws Exception {
	return super.authenticationManagerBean();
    }

    @Override
    @Bean
    public UserDetailsService userDetailsService() {
	UserDetailsService manager = new JpaUserDetailService();

	return manager;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

	// TODO: Review this security ...

	// @formatter:off

	httpSecurity.csrf().disable().authorizeRequests().antMatchers("/**")
	.permitAll().and().logout().permitAll();

//	httpSecurity
//		.csrf().disable()
//		.authorizeRequests()
//			.antMatchers("/favicon.ico").permitAll()
//			//.antMatchers("/logout_success").permitAll()
//			.antMatchers("login").permitAll()
//			
//
//		.and()
//		.formLogin()
//                	.loginPage("/login")
//                	.permitAll()
//                	.loginProcessingUrl("/login");
                //.and()
                //.addFilter(new SecurityContextPersistenceFilter()).securityContext();

	// @formatter:on

	httpSecurity.headers().frameOptions().disable();
    }

    @Autowired
    public void configGlobal(AuthenticationManagerBuilder auth)
	    throws Exception {

	auth.userDetailsService(userDetailsService())
		.passwordEncoder(passwordEncoder());

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
	PasswordEncoder encoder = new BCryptPasswordEncoder();
	return encoder;
    }
}
