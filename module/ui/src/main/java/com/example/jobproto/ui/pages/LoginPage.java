package com.example.jobproto.ui.pages;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wicketstuff.annotation.mount.MountPath;

import com.giffing.wicket.spring.boot.context.scan.WicketSignInPage;

/**
 * Login page class.
 *
 */
@WicketSignInPage
@MountPath("login")
public class LoginPage extends WebPage {

    private static Logger logger = LoggerFactory.getLogger(LoginPage.class);

    /**
     * Generated serial id.
     */
    private static final long serialVersionUID = 4850370394372851671L;

    /**
     * @param parameters
     * @see PageParameters
     */
    public LoginPage(PageParameters parameters) {
	super(parameters);
	add(new LoginForm("loginForm"));
	logger.info(parameters.toString());
	AuthenticatedWebSession.get().invalidateNow();
    }

    private class LoginForm extends StatelessForm<LoginForm> {

	/**
	 * Generated serial id.
	 */
	private static final long serialVersionUID = 6079898538002360672L;

	private String username;

	private String password;

	// private transient RequestCache requestCache = new
	// HttpSessionRequestCache();

	/**
	 * @param string
	 */
	public LoginForm(String id) {
	    super(id);
	    setModel(new CompoundPropertyModel<>(this));
	    add(new RequiredTextField<String>("username"));
	    add(new PasswordTextField("password"));
	    add(new FeedbackPanel("feedback"));
	}

	@Override
	protected void onSubmit() {
	    AuthenticatedWebSession session = AuthenticatedWebSession.get();
	    if (session.signIn(username, password)) {
		setResponsePage(getApplication().getHomePage());
	    } else {
		error("Login failed.");
	    }
	    ;

	}

    }

}
