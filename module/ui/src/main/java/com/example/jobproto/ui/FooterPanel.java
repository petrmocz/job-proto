/**
 *
 */
package com.example.jobproto.ui;

import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * TODO: comment this
 *
 * @since 2018-10-08
 */
public class FooterPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param id
     */
    public FooterPanel(String id) {
	super(id);

	add(new FeedbackPanel("feedback"));
    }

}
