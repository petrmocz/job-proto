/**
 *
 */
package com.example.jobproto.ui;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

import com.example.jobproto.ui.base.DockPanel;

/**
 * Home page class.
 */

@MountPath("homeold")
@AuthorizeInstantiation("USER")
public class HomePage extends TemplatePage {

    /**
     * Generated serial ID.
     */
    private static final long serialVersionUID = 7693948455142942796L;

    /**
     * @param parameters
     */
    public HomePage(PageParameters parameters) {
	super(parameters);

	Panel panel1 = new DockPanel("applicationPanel");
	add(panel1);
	Panel panel2 = new DockPanel("jobOfferPanel");
	add(panel2);
    }

}
