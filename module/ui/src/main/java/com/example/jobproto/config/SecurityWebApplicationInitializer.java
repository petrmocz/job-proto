package com.example.jobproto.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * TODO: comment this
 */
public class SecurityWebApplicationInitializer
	extends AbstractSecurityWebApplicationInitializer {

    public SecurityWebApplicationInitializer() {
	super(SecurityConfigurerAdapter.class);
    }

}