package com.example.jobproto.ui.pages;

import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.example.jobproto.ui.TemplatePage;

/**
 * Provider home page
 */
public class ProviderHomePage extends TemplatePage {

    private static final long serialVersionUID = -2745582266517232087L;

    /**
     *
     */
    public ProviderHomePage(PageParameters parameters) {
	super(parameters);
    }

}
