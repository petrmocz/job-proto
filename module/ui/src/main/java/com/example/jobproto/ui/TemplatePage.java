package com.example.jobproto.ui;

import java.util.logging.Level;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import lombok.extern.java.Log;

/**
 *
 * Default template page class.
 *
 *
 */
@Log
public class TemplatePage extends WebPage {

    private static final long serialVersionUID = -5771244403558037286L;

    public static final String CONTENT_ID = "contentComponent";

    private Component headerPanel = new HeaderPanel("headerPanel");
    private Component footerPanel = new FooterPanel("footerPanel");

    public TemplatePage(PageParameters parameters) {
	super(parameters);
	log.log(Level.WARNING, "Adding Header panel");
	add(headerPanel);
	log.log(Level.WARNING, "Adding Footer panel");
	add(footerPanel);
    }

    /**
     * @return the headerPanel
     */
    public Component getHeaderPanel() {
	return headerPanel;
    }

    /**
     * @param headerPanel
     *            the headerPanel to set
     */
    public void setHeaderPanel(Component headerPanel) {
	this.headerPanel = headerPanel;
    }

    /**
     * @return the footerPanel
     */
    public Component getFooterPanel() {
	return footerPanel;
    }

    /**
     * @param footerPanel
     *            the footerPanel to set
     */
    public void setFooterPanel(Component footerPanel) {
	this.footerPanel = footerPanel;
    }

}
