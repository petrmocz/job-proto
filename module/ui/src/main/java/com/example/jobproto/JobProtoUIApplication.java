package com.example.jobproto;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.example.jobproto.ui.WicketApplication;

/**
 * Spring Boot application start class.
 */
@SpringBootApplication
public class JobProtoUIApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
	new SpringApplicationBuilder().sources(WicketApplication.class)
		.profiles("dev").run(args);
    }

    @Override
    protected SpringApplicationBuilder configure(
	    SpringApplicationBuilder application) {
	return application.sources(WicketApplication.class);
    }

}