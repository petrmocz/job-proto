package com.example.jobproto.ui.base;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.ContextRelativeResource;

import com.example.jobproto.model.Applicant;
import com.example.jobproto.model.Skill;

/**
 * Person card
 */
public class ApplicantCard extends Card {

    /**
     * @param id
     *            Wicket ID
     * @param model
     *            Model of applicant @see {@link Applicant}
     */
    public ApplicantCard(String id, IModel<Applicant> model) {
	super(id, model);
	Applicant e = model.getObject();
	add(new Image("avatar", new ContextRelativeResource(e.getAvatar())));
	add(new Label("title", e.getFullName()));
	add(new Label("text", e.getDescription()));
	add(new Label("motto", e.getMotto()));

	List<Skill> skillList =
		e.getSkills().stream().collect(Collectors.toList());
	@SuppressWarnings("serial")
	ListView<Skill> skillListView =
		new ListView<Skill>("skillList", skillList) {

		    @Override
		    protected void populateItem(ListItem<Skill> item) {
			item.add(new Label("label",
				item.getModel().getObject().getSkillName()));

		    }
		};
	add(skillListView);
    }

}
