package com.example.jobproto.ui.base;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Abstract card class.
 *
 */
public abstract class Card extends Panel {

    private static final long serialVersionUID = -6987290807300059762L;

    /**
     * @param id
     *            Wicket id
     */
    public Card(String id) {
	super(id);
    }

    /**
     * @param id
     *            wicket id
     * @param model
     *            model
     */
    public Card(String id, IModel<?> model) {
	super(id, model);
    }

    // public Card(String id, String title, String text, String avatarResource)
    // {
    // super(id);
    //
    // add(new Image("avatar", new ContextRelativeResource(avatarResource)));
    // add(new Label("title", title));
    // add(new Label("text", text));
    //
    // }

}
