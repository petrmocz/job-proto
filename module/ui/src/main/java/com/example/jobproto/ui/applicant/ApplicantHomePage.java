package com.example.jobproto.ui.applicant;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

import com.example.jobproto.ui.TemplatePage;
import com.giffing.wicket.spring.boot.context.scan.WicketHomePage;

@WicketHomePage
@MountPath("home")
@AuthorizeInstantiation("USER")
public class ApplicantHomePage extends TemplatePage {

    private static final long serialVersionUID = -6833775851189380713L;

    public ApplicantHomePage(PageParameters parameters) {
	super(parameters);

	add(new ApplicantApplicationListPanel("applicantApplicationListPanel"));
	add(new ApplicantJobOffersListPanel("applicantJobOffersListPanel"));
    }

}