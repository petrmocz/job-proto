package com.example.jobproto.ui.applicant.userdetails;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

import com.example.jobproto.ui.TemplatePage;

/**
 * Applicant detail page
 *
 */
@MountPath("/userdetails")
public class ApplicantDetailPage extends TemplatePage {

    private static final long serialVersionUID = -964466791751055903L;

    public ApplicantDetailPage(PageParameters pageParameters) {
	super(pageParameters);

	add(new ApplicantGeneralDetailPanel("applicantGeneralDetailPanel"));
	add(new ApplicantSkillsPanel("applicantSkillsPanel"));
	add(new ApplicantJobHistoryPanel("applicantJobHistoryPanel"));

    }

}
