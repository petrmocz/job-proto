package com.example.jobproto.ui.base;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Basic UI panel.
 *
 */
public class AppPanel extends Panel {

    private static final long serialVersionUID = 8968143531870603523L;

    public AppPanel(String id) {
	super(id);
	add(new AttributeModifier("class", "panel"));
    }

}
