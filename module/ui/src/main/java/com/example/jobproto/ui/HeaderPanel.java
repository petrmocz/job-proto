package com.example.jobproto.ui;

import java.util.stream.Collectors;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Header panel class.
 */
public class HeaderPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param id
     */
    public HeaderPanel(String id) {
	super(id);

	add(new Label("username", SecurityContextHolder.getContext()
		.getAuthentication().getName()));
	add(new Label("usertype", SecurityContextHolder.getContext()
		.getAuthentication().getAuthorities().stream()
		.map(GrantedAuthority::getAuthority)
		.collect(Collectors.joining(","))));

    }

}
