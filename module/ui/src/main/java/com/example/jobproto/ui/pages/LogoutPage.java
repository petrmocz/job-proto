package com.example.jobproto.ui.pages;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

/**
 * Logout page class.
 *
 */
@MountPath("logout")
public class LogoutPage extends WebPage {

    private static final long serialVersionUID = -595577055702084197L;

    public LogoutPage(PageParameters parameters) {
	super(parameters);
	AuthenticatedWebSession.get().invalidateNow();
	// setResponsePage(LoginPage.class);
    }

}
